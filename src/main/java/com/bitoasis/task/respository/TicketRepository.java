package com.bitoasis.task.respository;

import com.bitoasis.task.entity.Ticket;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface Ticket repository.
 */
@Repository
public interface TicketRepository extends CrudRepository<Ticket, Long> {

    // any customization can be added here later on to query the data from the database level!
}
