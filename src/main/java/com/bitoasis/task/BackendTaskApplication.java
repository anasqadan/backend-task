package com.bitoasis.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * The type Backend task application.
 */
@EnableScheduling
@EnableFeignClients
@SpringBootApplication
public class BackendTaskApplication {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {

        SpringApplication.run(BackendTaskApplication.class, args);
    }

}
