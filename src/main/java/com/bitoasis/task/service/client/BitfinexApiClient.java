package com.bitoasis.task.service.client;

import com.bitoasis.task.config.FeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * The interface Bitfinex api client.
 */
@FeignClient(name = "bitfinex", url = "${client.bitfinex.url}", configuration = {FeignConfig.class})
public interface BitfinexApiClient {

    /**
     * The constant BITFINEX_TRADING_PATH.
     */
    String BITFINEX_TRADING_PATH = "/tBTCUSD";

    /**
     * Gets trading tickets.
     *
     * @return the trading tickets
     */
    @GetMapping(value = BITFINEX_TRADING_PATH)
    String getTradingTickets();

}
