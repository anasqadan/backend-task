package com.bitoasis.task.service.impl;

import com.bitoasis.task.entity.Ticket;
import com.bitoasis.task.respository.TicketRepository;
import com.bitoasis.task.service.TicketService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * The type Ticket service.
 */
@Service
@AllArgsConstructor
public class TicketServiceImpl implements TicketService {

    private final TicketRepository ticketRepository;

    @Override
    public Ticket createTicket(Ticket ticket) {

        ticketRepository.save(ticket);

        return ticket;
    }

    @Override
    public Optional<Ticket> findTicketById(Long ticketId) {

        return ticketRepository.findById(ticketId);
    }

    @Override
    public Optional<Ticket> updateTicket(Long ticketId, Ticket ticket) {

        Optional<Ticket> ticketData = ticketRepository.findById(ticketId);

        if (ticketData.isPresent()) {
            Ticket updatedTicket = ticketData.get();
            updatedTicket.setBid(ticket.getBid());
            updatedTicket.setBidSize(ticket.getBidSize());
            updatedTicket.setAsk(ticket.getAsk());
            updatedTicket.setAskSize(ticket.getAskSize());
            updatedTicket.setDailyChange(ticket.getDailyChange());
            updatedTicket.setDailyChangeRelative(ticket.getDailyChangeRelative());
            updatedTicket.setLastPrice(ticket.getLastPrice());
            updatedTicket.setVolume(ticket.getVolume());
            updatedTicket.setHigh(ticket.getHigh());
            updatedTicket.setLow(ticket.getLow());
            return Optional.of(ticketRepository.save(updatedTicket));
        }

        return Optional.empty();
    }

    @Override
    public void deleteTicketById(Long ticketId) {

        ticketRepository.deleteById(ticketId);

    }

    @Override
    public void deleteAll() {

        ticketRepository.deleteAll();
    }
}
