package com.bitoasis.task.service;

import com.bitoasis.task.entity.Ticket;

import java.util.Optional;

/**
 * The interface Ticket service.
 */
public interface TicketService {

    /**
     * Create ticket ticket.
     *
     * @param ticket the ticket
     * @return the ticket
     */
    Ticket createTicket(Ticket ticket);

    /**
     * Gets ticket by id.
     *
     * @param ticketId the ticket id
     * @return the ticket by id
     */
    Optional<Ticket> findTicketById(Long ticketId);

    /**
     * Update ticket optional.
     *
     * @param ticketId the ticket id
     * @param ticket   the ticket
     * @return the optional
     */
    Optional<Ticket> updateTicket(Long ticketId, Ticket ticket);

    /**
     * Delete ticket by id.
     *
     * @param ticketId the ticket id
     */
    void deleteTicketById(Long ticketId);

    /**
     * Delete all.
     */
    void deleteAll();
}
