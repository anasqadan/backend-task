package com.bitoasis.task.controller;

import com.bitoasis.task.entity.Ticket;
import com.bitoasis.task.service.TicketService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


/**
 * The type Ticket controller.
 */
@Log4j2
@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class TicketController {

    private final TicketService ticketService;

    /**
     * Gets ticket by ticketId.
     *
     * @param ticketId the ticketId
     * @return the ticket by ticketId
     */
    @GetMapping("/tickets/{ticketId}")
    public ResponseEntity<Ticket> getTicketById(@PathVariable("ticketId") long ticketId) {

        log.info("Retrieving ticket by ticketId {}", ticketId);

        Optional<Ticket> ticketData = ticketService.findTicketById(ticketId);

        if (ticketData.isPresent()) {
            return new ResponseEntity<>(ticketData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Create ticket response entity.
     *
     * @param ticket the ticket
     * @return the response entity
     */
    @PostMapping("/tickets")
    public ResponseEntity<Ticket> createTicket(@RequestBody Ticket ticket) {

        log.info("Creating a ticket: {}", ticket);

        try {
            ticketService.createTicket(ticket);
            return new ResponseEntity<>(ticket, HttpStatus.CREATED);
        } catch (Exception e) {

            log.error("Failed to create a ticket!", e);

            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update ticket response entity.
     *
     * @param ticketId the ticket id
     * @param ticket   the ticket
     * @return the response entity
     */
    @PutMapping("/tickets/{id}")
    public ResponseEntity<Ticket> updateTicket(@PathVariable("id") long ticketId, @RequestBody Ticket ticket) {

        log.info("Updating a ticket with  the following id: {}", ticketId);

        Optional<Ticket> ticketData = ticketService.updateTicket(ticketId, ticket);

        if (ticketData.isPresent()) {
            return new ResponseEntity(ticketData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Delete ticket response entity.
     *
     * @param ticketId the ticket id
     * @return the response entity
     */
    @DeleteMapping("/tickets/{id}")
    public ResponseEntity<HttpStatus> deleteTicket(@PathVariable("id") long ticketId) {

        log.info("Deleting a ticket that has the following id: {}", ticketId);

        try {
            ticketService.deleteTicketById(ticketId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {

            log.error("Failed to delete a ticket!", e);

            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Delete all tickets response entity.
     *
     * @return the response entity
     */
    @DeleteMapping("/tickets")
    public ResponseEntity<HttpStatus> deleteAllTickets() {

        log.info("Deleting all ticket:");

        try {
            ticketService.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {

            log.error("Failed to delete all ticket!", e);

            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}



