package com.bitoasis.task.config;

import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Feign config.
 */
@Configuration
public class FeignConfig {

    @Value("${client.bitfinex.application-content}")
    private String applicationContent;

    /**
     * The Authority.
     */
    @Value("${client.bitfinex.authority}")
    public String authority;

    /**
     * The User agent.
     */
    @Value("${client.bitfinex.user-agent}")
    public String userAgent;

    /**
     * The Accepted content.
     */
    @Value("${client.bitfinex.accepted-content}")
    public String acceptedContent;

    /**
     * Request interceptor request interceptor.
     *
     * @return the request interceptor
     */
    @Bean
    public RequestInterceptor requestInterceptor() {
        return requestTemplate -> {
            requestTemplate.header("Content-Type", applicationContent);
            requestTemplate.header("authority", authority);
            requestTemplate.header("user-agent", userAgent);
            requestTemplate.header("accept", acceptedContent);
        };
    }
}