package com.bitoasis.task.task;


import com.bitoasis.task.entity.Ticket;
import com.bitoasis.task.service.TicketService;
import com.bitoasis.task.service.client.BitfinexApiClient;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * The type Fetching ticket scheduled task.
 */
@Log4j2
@Component
@AllArgsConstructor
public class FetchingTicketScheduledTask {

    public static final String REGEX_EXPRESSION = "[^a-zA-Z0-9\\.,]";
    public static final String COMMA = ",";

    private final BitfinexApiClient bitfinexApiClient;
    private final TicketService ticketService;


    // can be added in a more dynamic way, but will make it fix right now as it's required
    @Scheduled(fixedDelay = 10000)
    private void fetchTicket() {
        String jsonString = bitfinexApiClient.getTradingTickets();

        //short return
        Optional<Ticket> ticket = parseTicket(jsonString);
        if (!ticket.isPresent()) {
            log.error("Error in parsing ticket that has the following information:" + jsonString);
            return;
        }

        ticketService.createTicket(ticket.get());

        log.info("Ticket that has the following id {} has been stored!", ticket.get().getId());
    }

    private Optional<Ticket> parseTicket(String jsonString) {
        String[] ticketArray = jsonString.replaceAll(REGEX_EXPRESSION, "").split(COMMA);
        return getTicket(ticketArray);
    }

    private Optional<Ticket> getTicket(String[] ticketArray) {

        //short return
        if (ticketArray.length == 0) {
            // return it empty as it doesn't match what expected!
            return Optional.empty();
        }

        // creating a ticket with the all needed attributes
        Ticket ticket = new Ticket();
        ticket.setBid((ticketArray[0]));
        ticket.setBidSize((ticketArray[1]));
        ticket.setAsk((ticketArray[2]));
        ticket.setAskSize((ticketArray[3]));
        ticket.setDailyChange((ticketArray[4]));
        ticket.setDailyChangeRelative((ticketArray[5]));
        ticket.setLastPrice((ticketArray[6]));
        ticket.setVolume((ticketArray[7]));
        ticket.setHigh((ticketArray[8]));
        ticket.setLow((ticketArray[9]));

        // return the ticket back!
        return Optional.of(ticket);
    }
}
