package com.bitoasis.task.entity;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


/**
 * The type Ticket.
 */
@Entity(name = "ticket")    // This tells Hibernate to make a table out of this class
@Data   // to get and set attributes, has code and toString, using @data will do that job
@NoArgsConstructor  // no need to create constructor to set what attribute is need to.
public class Ticket {

    @Id
    @SequenceGenerator(name = "seq", sequenceName = "seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    private Long id;

    @Column(name = "bid")
    private String bid;

    @Column(name = "bid_size")
    private String bidSize;

    @Column(name = "ask")
    private String ask;

    @Column(name = "ask_size")
    private String askSize;

    @Column(name = "daily_change")
    private String dailyChange;

    @Column(name = "daily_change_relative")
    private String dailyChangeRelative;

    @Column(name = "last_price")
    private String lastPrice;

    @Column(name = "volume")
    private String volume;

    @Column(name = "high")
    private String high;

    @Column(name = "low")
    private String low;

}



