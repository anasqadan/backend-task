package com.bitoasis.task;

import com.bitoasis.task.entity.Ticket;
import com.bitoasis.task.exception.ResourceNotFoundException;
import com.bitoasis.task.respository.TicketRepository;
import com.bitoasis.task.service.impl.TicketServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * The type Backend task application tests.
 */
@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class BackendTaskApplicationTests {

    @Mock
    private TicketRepository ticketRepository;

    @InjectMocks
    private TicketServiceImpl ticketService;

    /**
     * Should return same ticket.
     */
    @Test
    public void shouldReturnSameTicket() {

        Long ticketId = 107L;
        Optional<Ticket> ticket = Optional.empty();
        given(ticketRepository.findById(ticketId)).willReturn(ticket);
        Optional<Ticket> expected = ticketService.findTicketById(ticketId);
        assertEquals(expected, ticket);
    }

    /**
     * When given id should delete ticket if found.
     */
    @Test
    public void whenGivenId_shouldDeleteTicket_ifFound() {

        Ticket ticket = new Ticket();
        ticket.setId(108L);
        when(ticketRepository.findById(ticket.getId())).thenReturn(Optional.of(ticket));
        ticketService.deleteTicketById(ticket.getId());
        verify(ticketRepository).deleteById(ticket.getId());
    }

    /**
     * When delete id should optional ticket empty returned.
     */
    @Test
    public void whenDeleteId_shouldOptionalTicketEmpty_returned() {

        Ticket ticket = new Ticket();
        ticket.setId(109L);
        ticketService.deleteTicketById(ticket.getId());
        given(ticketRepository.findById(ticket.getId())).willReturn(Optional.empty());
    }

    /**
     * When create ticket returned not null.
     */
    @Test
    public void whenCreateTicket_returnedNotNull() {

        Ticket ticket = new Ticket();

        ticket.setBid("35593");
        ticket.setBidSize("19.87556967");
        ticket.setAsk("35594");
        ticket.setAskSize("15.959811649999997");
        ticket.setDailyChange("-1175");
        ticket.setDailyChangeRelative("-0.032");
        ticket.setLastPrice("35594");
        ticket.setVolume("7573.71306583");
        ticket.setLow("37502");
        ticket.setHigh("34630");

        given(ticketRepository.save(ticket)).willReturn(ticket);
        assertNotNull(ticketService.createTicket(ticket));
    }


    /**
     * When update ticket returned not null.
     */
    @Test
    public void whenUpdateTicket_returnedNotNull() {

        Ticket ticket = new Ticket();
        ticket.setId(105L);
        when(ticketRepository.findById(ticket.getId())).thenReturn(Optional.of(ticket));
        Optional<Ticket> ticketById = ticketService.findTicketById(ticket.getId());

        if (!ticketById.isPresent()) {
            throw new ResourceNotFoundException("Ticket with the following id is not present " + ticket.getId());
        }

        Ticket updatedTicket = ticketById.get();
        updatedTicket.setBid("35593");
        updatedTicket.setAsk("35594");
        updatedTicket.setDailyChange("-1175");
        updatedTicket.setVolume("7573.71306583");
        updatedTicket.setLow("37502");

        given(ticketRepository.save(updatedTicket)).willReturn(updatedTicket);
        assertNotNull(ticketService.updateTicket(ticket.getId(), updatedTicket));
    }

}
