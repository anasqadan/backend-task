# README #

### What is this repository for? ###

This is to retrieve a ticket and store it in PostgresSql Database, and do some operations over the ticketing data using the available APIs exposed on this application.

### Configuration needed to be done over the application before starting ###

* Check that port 8080 is available in your machine, otherwise, try to change the port of this application to another free port
* Check if PostgresSql connection that mentioned in the application.properties if it matches yours, otherwise; change them to make them proper

### Swagger UI? ###

To reach swagger; you can follow the following link http:localhost:${port}/swagger-ui/index.html 

* in default case here 
http://localhost:8080/swagger-ui/index.html

### Who do I talk to? ###

* This is Repository is added and pushed by @ Anas Quedan
if you have any comments, feel free to reach me out via [Anas Quedan Email](mailto:anas.quedan@gmail.com) 